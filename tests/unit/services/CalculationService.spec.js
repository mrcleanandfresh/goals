import {CalculationModel} from "../../../src/components/Calculator";
import {CalculationType} from "../../../src/components/Calculation";
import * as CalculationService from "../../../src/services/CalculationService";
import {calculate, calculateInterestPaymentForCreditCard} from "../../../src/services/CalculationService";
import CleaningService from "../../../src/services/CleaningService";
import {$ as m$, in$} from 'moneysafe';
import {Decimal} from 'decimal.js';
import {calculateInterestPaymentForAuto} from "../../../src/services/CalculationService";

jest.unmock("../../../src/services/CalculationService");
jest.mock("../../../src/services/CleaningService");

describe('CalculationService tests', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should calculate interest payment', () => {
        const interestPayment = CalculationService.calculateInterestPayment(100, 3);
        expect(interestPayment).toBe(0.25);
    });

    it('should calculate principle payment', () => {
        const principalPayment = CalculationService.calculatePrincipalPayment(100, 10);
        expect(principalPayment).toBe(90);
    });

    it('should calculate new balance', () => {
        const newBalance = CalculationService.calculateNewBalance(100, 10);
        expect(newBalance).toBe(90);
    });

    it('should map calculations', () => {
        let value = new CalculationModel();

        let mapped = CalculationService.mapCalculations([value]);
        expect(CleaningService.cleanIfString).toHaveBeenCalled();
        expect(mapped.length).toBe(1);
        expect(mapped[0].type).toBe(value.type);
        expect(mapped[0].name).toBe(`${value.subType} 1`);
        expect(mapped[0].amortization.length).toBe(0);
        expect(mapped[0].payments.length).toBe(0);
        expect(mapped[0].months).toBe(0);

        CleaningService.cleanIfString.mockReset();
        value.type = CalculationType.SAVINGS;
        mapped = CalculationService.mapCalculations([value]);

        expect(CleaningService.cleanIfString).toHaveBeenCalled();
        expect(mapped.length).toBe(1);
        expect(mapped[0].type).toBe(value.type);
        expect(mapped[0].name).toBe('Savings 1');
        expect(mapped[0].amortization.length).toBe(0);
        expect(mapped[0].payments.length).toBe(0);
        expect(mapped[0].months).toBe(0);
    });

    it('should calculate results for LOAN', () => {
        CleaningService.cleanIfString.mockImplementation((value) => {
            if (typeof value === 'string') {
                return Number(value.replace(/[^0-9.]/g, ''));
            }
            return value;
        });

        const loan1 = new CalculationModel();
        loan1.balance = '8,000';
        loan1.interestRate = 24;
        loan1.payment = '220';

        const loan2 = new CalculationModel();
        loan2.balance = '7,000';
        loan2.interestRate = 14;
        loan2.payment = '135';

        const loan3 = new CalculationModel();
        loan3.balance = '1,500';
        loan3.interestRate = 20;
        loan3.payment = '75';

        const calculations = [loan1, loan2, loan3];
        const actual = calculate(calculations);

        for (let i = 0; i < actual.length; ++i) {
            const principal = actual[i].amortization.reduce((sum, item) => (in$(m$(sum) + m$(item.principalPayment))), m$(0));
            const interest = actual[i].amortization.reduce((sum, item) => (in$(m$(sum) + m$(item.interestPayment))), m$(0));

            // expect that principal paid equals the loan balance
            expect(principal).toBe(actual[i].balance);

            // expect the total to be more than the balance
            expect(principal + interest).toBeGreaterThan(actual[i].balance);

            // expect the amount of iterations and total months to match, plus one, because we start on the first of the next month.
            expect(actual[i].months).toEqual(actual[i].amortization.length + 1);

            // expect us to find our own payment in the payments.
            expect(actual[i].payments.find(payment => payment.value === actual[i].payment)).toBeDefined();
        }

        // since it's sorted by payments, the last item should have the combined payments of all calculations
        expect(actual[actual.length - 1].payments.length).toEqual(calculations.length);
        expect(CleaningService.cleanIfString).toHaveBeenCalled();
    });

    it('should calculate results for SAVINGS', () => {
        CleaningService.cleanIfString.mockImplementation((value) => {
            if (typeof value === 'string') {
                return Number(value.replace(/[^0-9.]/g, ''));
            }
            return value;
        });

        const savings1 = new CalculationModel();
        savings1.type = CalculationType.SAVINGS;
        savings1.balance = '8,000';
        savings1.goal = '10,000';
        savings1.payment = '220';

        const savings2 = new CalculationModel();
        savings2.type = CalculationType.SAVINGS;
        savings2.balance = '1,500';
        savings2.goal = '3,000';
        savings2.payment = '135';

        const savings3 = new CalculationModel();
        savings3.type = CalculationType.SAVINGS;
        savings3.balance = '1,500';
        savings3.goal = '3,000';
        savings3.payment = '135';

        const calculations = [savings1, savings2, savings3];
        const actual = calculate(calculations);

        for (let i = 0; i < actual.length; ++i) {
            // expect that principal paid will be greater or equal to balance; cuz more money saved isn't a bad thing.
            expect(actual[i].goal).toBeGreaterThanOrEqual(actual[i].balance);

            // expect the amount of iterations and total months to match, plus one, because we start on the first of the next month.
            expect(actual[i].months).toEqual(actual[i].amortization.length + 1);

            // expect us to find our own payment in the payments.
            expect(actual[i].payments.find(payment => payment.value === actual[i].payment)).toBeDefined();
        }

        // since it's sorted by payments, the last item should have the combined payments of all calculations
        expect(actual[actual.length - 1].payments.length).toEqual(calculations.length);
    });

    it('should calculate results for LOAN and break the combined payments rule, if LOANs are the same', () => {
        const loan1 = new CalculationModel();
        loan1.balance = '7,000';
        loan1.interestRate = 14;
        loan1.payment = '135';

        const loan2 = new CalculationModel();
        loan2.balance = '1,500';
        loan2.interestRate = 20;
        loan2.payment = '75';

        const loan3 = new CalculationModel();
        loan3.balance = '1,500';
        loan3.interestRate = 20;
        loan3.payment = '75';

        let calculations = [loan1, loan2, loan3];
        let actual = calculate(calculations);

        // since it's sorted by payments, the last item should have the combined payments of all calculations
        // except in this instance where there are loans with the exact same interest rate, balance and payment.
        // This does not apply to the SAVINGS calculation type.
        expect(actual[actual.length - 1].payments.length).toEqual(calculations.length - 1);
    });

    it('should calculate interest for credit card', () => {
        let expected = 6.75;
        let actual = calculateInterestPaymentForCreditCard(750, 11, 30);
        expect(actual).toEqual(expected);
        expected = 12.30;
        actual = calculateInterestPaymentForCreditCard(1000, 15, 30);
        expect(actual).toEqual(expected);
    });

    it('should calculate interest for auto loan', () => {
        let expected = 141.67;
        let actual = calculateInterestPaymentForAuto(20000, 8.5);
        expect(actual).toEqual(expected);
        expected = 138.20;
        actual = calculateInterestPaymentForAuto(19510.32, 8.5);
        expect(actual).toEqual(expected);
    });

    it('should calculate monthly payment for auto loan', () => {
        // see https://www.wikihow.com/Calculate-Total-Interest-Paid-on-a-Car-Loan#Computing-Total-Interest-Using-The-Simple-Interest-Formula_sub
        const principal = new Decimal(10000);
        const monthlyInterestRate = new Decimal(6 / 12).dividedBy(100);
        const numOfPayments = 6 * 12;

        const totalInterest = principal.mul(
            monthlyInterestRate.mul(Decimal.pow(new Decimal(1).add(monthlyInterestRate), numOfPayments)).dividedBy(
                Decimal.pow(new Decimal(1).add(monthlyInterestRate), numOfPayments).sub(1)
            )
        );

        expect(parseFloat(totalInterest.toFixed(2))).toEqual(165.73);
    })
});

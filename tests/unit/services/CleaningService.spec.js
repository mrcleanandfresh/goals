import CleaningService from "../../../src/services/CleaningService";

describe('CleaningService tests', () => {
    it('should clean value', () => {
        let value = '';
        expect(CleaningService.clean(value)).toBe(value);
        value = '500';
        expect(CleaningService.clean(value)).toBe(500);
        value = (500).toFixed(2);
        expect(CleaningService.clean(value)).toBe(500.00);
        value = '$100';
        expect(CleaningService.clean(value)).toBe(100);
    });

    it(`it should check if it's a string or not`, () => {
        let value = '', actual;
        const clean = CleaningService['clean'] = jest.fn();

        actual = CleaningService.cleanIfString(value);
        expect(actual).toBeUndefined(); // because it's mocked.
        expect(clean).toHaveBeenCalledWith(value);
        value = 10;
        clean.mockClear();
        actual = CleaningService.cleanIfString(value);
        expect(actual).toEqual(value);
        expect(clean).not.toHaveBeenCalled();
        value = 10.00;
        clean.mockClear();
        actual =  CleaningService.cleanIfString(value);
        expect(actual).toEqual(value);
        expect(clean).not.toHaveBeenCalled();
    })
});

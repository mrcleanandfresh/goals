import {shallowMount} from "@vue/test-utils";
import CalculationStatsTotal from "../../../src/components/CalculationStatsTotal";


describe('CalculationStatsTotal.vue tests', () => {
    it('hides interest text if less than 5, but still shows bar', () => {
        const wrapper = shallowMount(CalculationStatsTotal, {
            propsData: {
                interestSegment: 4,
                principalSegment: 96
            }
        });

        expect(wrapper.html()).not.toContain('4% Interest');
        expect(wrapper.find('.bg-secondary').html()).toContain('width');
    })
});

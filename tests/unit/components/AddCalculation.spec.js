import Vue from 'vue';
import AddCalculation from '../../../src/components/AddCalculation';
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {mount} from "@vue/test-utils";

library.add(faPlus);
Vue.component('font-awesome-icon', FontAwesomeIcon);

describe(`AddCalculation Component Tests`, () => {
    // Inspect the component instance on mount
    it('correctly sets the message when created', () => {
        const spy = jest.fn();
        const wrapper = mount(AddCalculation, {
            methods: {
                addCalculation: spy
            }
        });
        const button = wrapper.find('button');
        button.trigger('click');
        expect(spy).toHaveBeenCalled();
    });
});

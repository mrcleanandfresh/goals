import {shallowMount} from "@vue/test-utils";
import CalculationResult from './../../../src/components/CalculationResult';
import CalculationBreakdown from "../../../src/components/CalculationBreakdown";
import moment from "moment";
import CalculationStatsTotal from "../../../src/components/CalculationStatsTotal";
import {library} from "@fortawesome/fontawesome-svg-core";
import Vue from "vue";
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {faPrint} from "@fortawesome/free-solid-svg-icons/faPrint";

library.add(faPrint);
Vue.component('font-awesome-icon', FontAwesomeIcon);

describe('CalculationResult.vue tests', () => {
    it('should create a CalculationBreakdown if there are results', () => {
        const wrapper = shallowMount(CalculationResult, {
            propsData: {
                results: [
                    {
                        name: 'test',
                        amortization: [],
                        endMoment: moment()
                    }
                ]
            }
        });

        expect(wrapper.findAll(CalculationBreakdown).length).toBe(1);
    });
    describe('accurately calculate', () => {
        it('should accurately calculate total principal', () => {
            const wrapper = shallowMount(CalculationResult, {
                propsData: {
                    results: [
                        {
                            name: 'test',
                            amortization: [
                                {principalPayment: 5},
                                {principalPayment: 5},
                                {principalPayment: 5},
                            ],
                            endMoment: moment()
                        }
                    ]
                }
            });

            const stats = wrapper.find(CalculationStatsTotal);

            expect(stats.props().totalPrincipal).toBe('15');
        });

        it('should accurately calculate interest', () => {
            const wrapper = shallowMount(CalculationResult, {
                propsData: {
                    results: [
                        {
                            name: 'test',
                            amortization: [
                                {interestPayment: 5},
                                {interestPayment: 5.5}, // rounding up
                                {interestPayment: 5},
                            ],
                            endMoment: moment()
                        }
                    ]
                }
            });

            const stats = wrapper.find(CalculationStatsTotal);

            expect(stats.props().totalInterest).toBe('16');
        });

        it('should accurately calculate principal segment', () => {
            const result = {
                name: 'test',
                amortization: [
                    {principalPayment: 5, interestPayment: 5},
                    {principalPayment: 5, interestPayment: 5},
                    {principalPayment: 5, interestPayment: 5},
                ],
                endMoment: moment()
            };

            const wrapper = shallowMount(CalculationResult, {
                propsData: {
                    results: [result]
                }
            });

            const stats = wrapper.find(CalculationStatsTotal);

            const expected = 50;

            expect(stats.props().principalSegment).toBe(expected);
        });

        it('should accurately calculate interest segment', () => {
            const result = {
                name: 'test',
                amortization: [
                    {principalPayment: 5, interestPayment: 5},
                    {principalPayment: 5, interestPayment: 5},
                    {principalPayment: 5, interestPayment: 5},
                ],
                endMoment: moment()
            };

            const wrapper = shallowMount(CalculationResult, {
                propsData: {
                    results: [result]
                }
            });

            const stats = wrapper.find(CalculationStatsTotal);

            const expected = 50;

            expect(stats.props().interestSegment).toBe(expected);
        });

        it('should accurately choose the greatest date', () => {
            const result1 = {
                name: 'test',
                amortization: [
                    {principalPayment: 5, interestPayment: 5},
                    {principalPayment: 5, interestPayment: 5},
                    {principalPayment: 5, interestPayment: 5},
                ],
                endMoment: moment().add(2, 'months')
            };
            const result2 = {
                name: 'test',
                amortization: [
                    {principalPayment: 5, interestPayment: 5},
                    {principalPayment: 5, interestPayment: 5},
                    {principalPayment: 5, interestPayment: 5},
                ],
                endMoment: moment().add(4, 'months')
            };

            const wrapper = shallowMount(CalculationResult, {
                propsData: {
                    results: [result1, result2]
                }
            });

            const stats = wrapper.find(CalculationStatsTotal);

            expect(stats.props().furthestEndMoment).toBe(result2.endMoment);
        });
    });
});

import Navigation from "../../../src/components/Navigation";
import {shallowMount} from "@vue/test-utils";

describe('Navigation.vue tests', () => {
    it('should have the site title', () => {
        const wrapper = shallowMount(Navigation);

        expect(wrapper.html()).toContain('Goals');
    })
});

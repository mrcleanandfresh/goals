import Calculator, {CalculationModel} from "../../../src/components/Calculator";
import Calculation, {CalculationType} from "../../../src/components/Calculation";
import {shallowMount} from "@vue/test-utils";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faPlus, faSortAmountUpAlt} from "@fortawesome/free-solid-svg-icons";
import {faTimes} from "@fortawesome/free-solid-svg-icons/faTimes";
import {faSortAmountDownAlt} from "@fortawesome/free-solid-svg-icons/faSortAmountDownAlt";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons/faArrowLeft";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons/faArrowRight";
import Vue from 'vue';
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import CalculationResult from "../../../src/components/CalculationResult";
import ValidationService from "./../../../src/services/ValidationService";
import CleaningService from "./../../../src/services/CleaningService";
import arrayMove from 'array-move';
jest.mock('./../../../src/services/ValidationService');
jest.mock('./../../../src/services/CleaningService');
jest.mock('array-move');

library.add(faPlus, faTimes, faSortAmountDownAlt, faSortAmountUpAlt, faArrowLeft, faArrowRight);
Vue.component('font-awesome-icon', FontAwesomeIcon);

const interestRateFactory = (interestRate) => {
    const model = new CalculationModel();
    model.interestRate = interestRate;
    return model;
};

const balanceFactory = (balance) => {
    const model = new CalculationModel();
    model.balance = balance;
    return model;
};

const calculationTypeFactory = (type) => {
    const model = new CalculationModel();
    model.type = type;
    return model;
};

describe('Calculator.vue tests', () => {
    it('should show a message if it has an error', () => {
        const message = "You have an error message";
        const wrapper = shallowMount(Calculator, {
            propsData: {
                hasError: true,
                message
            }
        });
        expect(wrapper.html()).toContain(message);
    });

    it('should show a CalculationResult if there are results', () => {
        const wrapper = shallowMount(Calculator, {
            propsData: {
                results: [{}]
            }
        });

        expect(wrapper.find(CalculationResult)).toBeTruthy();
    });

    describe('@events', () => {
        afterEach(() => {
            jest.clearAllMocks();
        });

        it('@click should sort calculations by lowest interest rate', () => {
            const twentyFive = interestRateFactory(25);
            const fifty = interestRateFactory(50);
            const seventyFive = interestRateFactory(75);
            const oneHundred = interestRateFactory(100);

            const calculationsBefore = [
                twentyFive,
                seventyFive,
                oneHundred,
                fifty,
            ];

            const calculationsAfter = [
                twentyFive,
                fifty,
                seventyFive,
                oneHundred,
            ];

            CleaningService.cleanIfString.mockImplementation((value) => {
                return value;
            });

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: calculationsBefore
                })
            });

            const buttons = wrapper.findAll('.sort-button').wrappers;
            const button = buttons.filter((wrapper) => (
                wrapper.html().toLowerCase().includes('lowest interest')
            )).pop();
            button.trigger('click');
            expect(wrapper.vm.$data.calculations).toEqual([...calculationsAfter]);
        });

        it('@click should sort calculations by highest interest rate', () => {
            const twentyFive = interestRateFactory(25);
            const fifty = interestRateFactory(50);
            const seventyFive = interestRateFactory(75);
            const oneHundred = interestRateFactory(100);

            const calculationsBefore = [
                twentyFive,
                seventyFive,
                oneHundred,
                fifty,
            ];

            const calculationsAfter = [
                oneHundred,
                seventyFive,
                fifty,
                twentyFive,
            ];

            CleaningService.cleanIfString.mockImplementation((value) => {
                return value;
            });

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: calculationsBefore
                })
            });

            const buttons = wrapper.findAll('.sort-button').wrappers;
            const button = buttons.filter((wrapper) => (
                wrapper.html().toLowerCase().includes('highest interest')
                )).pop();
            button.trigger('click');
            expect(wrapper.vm.$data.calculations).toEqual([...calculationsAfter]);
        });

        it('@click should sort calculations by lowest balance', () => {
            const twentyFiveHundred = balanceFactory('2,500');
            const fiveThousand = balanceFactory('5,000');
            const seventyFiveHundred = balanceFactory('7,500');
            const tenThousand = balanceFactory('10,000');

            const calculationsBefore = [
                twentyFiveHundred,
                seventyFiveHundred,
                tenThousand,
                fiveThousand,
            ];

            const calculationsAfter = [
                twentyFiveHundred,
                fiveThousand,
                seventyFiveHundred,
                tenThousand,
            ];

            CleaningService.cleanIfString.mockImplementation(value => {
                 return Number(value.replace(/[^0-9.]/g, ''));
            });

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: calculationsBefore
                })
            });

            const buttons = wrapper.findAll('.sort-button').wrappers;
            const button = buttons.filter((wrapper) => (
                wrapper.html().toLowerCase().includes('lowest balance'))
            ).pop();
            button.trigger('click');
            expect(wrapper.vm.$data.calculations).toEqual([...calculationsAfter]);
        });

        it('@click should sort calculations by highest balance', () => {
            const twentyFiveHundred = balanceFactory('2,500');
            const fiveThousand = balanceFactory('5,000');
            const seventyFiveHundred = balanceFactory('7,500');
            const tenThousand = balanceFactory('10,000');

            const calculationsBefore = [
                twentyFiveHundred,
                seventyFiveHundred,
                tenThousand,
                fiveThousand,
            ];

            const calculationsAfter = [
                tenThousand,
                seventyFiveHundred,
                fiveThousand,
                twentyFiveHundred,
            ];

            CleaningService.cleanIfString.mockImplementation(value => {
                return Number(value.replace(/[^0-9.]/g, ''));
            });

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: calculationsBefore
                })
            });

            const buttons = wrapper.findAll('.sort-button').wrappers;
            const button = buttons.filter((wrapper) => (
                wrapper.html().toLowerCase().includes('highest balance'))
            ).pop();
            button.trigger('click');
            expect(wrapper.vm.$data.calculations).toEqual([...calculationsAfter]);
        });

        it('@click should run calculate when calculate is clicked', () => {
            // TODO Calculate Button: implement this complicated test
        });


        it('@type-change should change calculation type for single calculation', () => {
            const calculation = calculationTypeFactory(CalculationType.LOAN);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onCalculationTypeChange(CalculationType.SAVINGS, 0);
            const calculationComp = wrapper.find(Calculation);

            expect(calculationComp.props().calculation.type).toEqual(CalculationType.SAVINGS);
        });


        it('@balance-change for loan should change balance, clean value, and validate payment', () => {
            const calculation = calculationTypeFactory(CalculationType.LOAN);
            const value = '5,000';
            const payment = '200';
            calculation.payment = payment;

            ValidationService.validateAndClean.mockImplementation(() => 5000);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onPaymentChange = jest.fn();
            wrapper.vm.onBalanceChange(value, CalculationType.LOAN, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.balance).toEqual(value);
            // it should validate payment after valid balance
            expect(wrapper.vm.onPaymentChange).toHaveBeenCalledWith(payment, CalculationType.LOAN, 0);
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
        });

        it('@balance-change for savings should change balance, clean value, and validate payment', () => {
            const calculation = calculationTypeFactory(CalculationType.SAVINGS);
            const value = '5,000';
            const payment = '200';
            calculation.payment = payment;

            ValidationService.validateAndClean.mockImplementation(() => 5000);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onPaymentChange = jest.fn();
            wrapper.vm.onBalanceChange(value, CalculationType.SAVINGS, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.balance).toEqual(value);
            // it should validate payment after valid balance
            expect(wrapper.vm.onPaymentChange).toHaveBeenCalledWith(payment, CalculationType.SAVINGS, 0);
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
        });

        it('@balance-change should not validate payment if invalid', () => {
            const calculation = calculationTypeFactory(CalculationType.SAVINGS);
            const value = '5,000';

            ValidationService.validateAndClean.mockImplementation(() => false);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onPaymentChange = jest.fn();
            wrapper.vm.onBalanceChange(value, CalculationType.SAVINGS, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.balance).toEqual(value);
            // it should validate payment after valid balance
            expect(wrapper.vm.onPaymentChange).not.toHaveBeenCalled();
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
        });


        it('@interest-rate-change should change interest rate', () => {
            const value = 15;
            const calculation = interestRateFactory(value);

            ValidationService.validateAndClean.mockImplementation(() => value);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onInterestRateChange(value, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.interestRate).toEqual(value);
            // it should set the calculation on success
            expect(wrapper.vm.calculation).not.toBe(calculation); // Object.is equality
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
        });

        it('@interest-rate-change should validate', () => {
            const value = 15;
            const calculation = interestRateFactory(value);

            ValidationService.validateAndClean.mockImplementation(() => false);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onInterestRateChange(value, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.interestRate).toEqual(value);
            // it should set the calculation even on failure
            expect(wrapper.vm.calculation).not.toBe(calculation); // Object.is equality
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
        });


        it('@payment-change for loan should change payment', () => {
            const calculation = calculationTypeFactory(CalculationType.LOAN);
            const value = '5,000';
            const cleanedValue = 5000;

            const payment = '200';
            const cleanedPayment = 200;

            calculation.payment = payment;
            calculation.balance = value; // because it's a loan

            ValidationService.validateAndClean.mockImplementation(() => cleanedPayment);
            CleaningService.clean.mockImplementation(() => cleanedValue);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onPaymentChange(payment, CalculationType.LOAN, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change payment
            expect(calculationComp.props().calculation.payment).toEqual(payment);
            // it should set the calculation on success
            expect(wrapper.vm.calculation).not.toBe(calculation); // Object.is equality
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
            expect(CleaningService.cleanIfString).toHaveBeenCalledWith(value);
        });

        it('@payment-change for savings should change payment', () => {
            const calculation = calculationTypeFactory(CalculationType.SAVINGS);
            const value = '5,000';
            const cleanedValue = 5000;

            const payment = '200';
            const cleanedPayment = 200;

            calculation.payment = payment;
            calculation.goal = value; // because it's a loan

            ValidationService.validateAndClean.mockImplementation(() => cleanedPayment);
            CleaningService.clean.mockImplementation(() => cleanedValue);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onPaymentChange(payment, CalculationType.SAVINGS, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.payment).toEqual(payment);
            // it should set the calculation on success
            expect(wrapper.vm.calculation).not.toBe(calculation); // Object.is equality
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
            expect(CleaningService.cleanIfString).toHaveBeenCalledWith(value);
        });

        it('@payment-change should validate', () => {
            const calculation = calculationTypeFactory(CalculationType.SAVINGS);
            const value = '5,000';
            const cleanedValue = 5000;

            const payment = '200';

            calculation.payment = payment;
            calculation.goal = value; // because it's a loan

            ValidationService.validateAndClean.mockImplementation(() => false);
            CleaningService.clean.mockImplementation(() => cleanedValue);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onPaymentChange(payment, CalculationType.SAVINGS, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.payment).toEqual(payment);
            // it should set the calculation on success
            expect(wrapper.vm.calculation).not.toBe(calculation); // Object.is equality
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
            expect(CleaningService.cleanIfString).toHaveBeenCalledWith(value);
        });


        it('@goal-change should change goal', () => {
            const calculation = calculationTypeFactory(CalculationType.SAVINGS);
            const value = '5,000';
            const cleanedValue = 5000;
            const payment = '200';
            calculation.goal = value;
            calculation.payment = payment;

            ValidationService.validateAndClean.mockImplementation(() => cleanedValue);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onPaymentChange = jest.fn();
            wrapper.vm.onGoalChange(value, CalculationType.SAVINGS, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.goal).toEqual(value);
            // it should validate payment after valid balance
            expect(wrapper.vm.onPaymentChange).toHaveBeenCalledWith(payment, CalculationType.SAVINGS, 0);
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
        });

        it('@goal-change should validate', () => {
            const calculation = calculationTypeFactory(CalculationType.SAVINGS);
            const value = '5,000';

            ValidationService.validateAndClean.mockImplementation(() => false);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onPaymentChange = jest.fn();
            wrapper.vm.onGoalChange(value, CalculationType.SAVINGS, 0);
            const calculationComp = wrapper.find(Calculation);

            // it should change balance
            expect(calculationComp.props().calculation.goal).toEqual(value);
            // it should validate payment after valid balance
            expect(wrapper.vm.onPaymentChange).not.toHaveBeenCalled();
            // it should validate
            expect(ValidationService.validateAndClean).toHaveBeenCalled();
        });


        it('@remove-calculation should remove the given calculation', () => {
            const calculation = calculationTypeFactory(CalculationType.LOAN);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onRemoveCalculation(0);

            expect(wrapper.vm.$data.calculations.length).toBe(0);
        });

        it('@added-calculation should add a new Calculation Model', () => {
            const calculation = calculationTypeFactory(CalculationType.LOAN);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation]
                })
            });

            wrapper.vm.onAddCalculation(0);

            expect(wrapper.vm.$data.calculations[1]).toBeTruthy();
        });


        it('@change-position moves items from one position to another', () => {
            const calculation1 = calculationTypeFactory(CalculationType.LOAN);
            const calculation2 = calculationTypeFactory(CalculationType.LOAN);

            const wrapper = shallowMount(Calculator, {
                data: () => ({
                    calculations: [calculation1, calculation2]
                })
            });

            wrapper.vm.onPositionChange(1, 0);

            expect(arrayMove).toHaveBeenCalledWith([calculation1, calculation2], 1, 0);
        });
    });
});

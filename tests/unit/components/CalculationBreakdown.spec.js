import {shallowMount} from "@vue/test-utils";
import CalculationBreakdown from '../../../src/components/CalculationBreakdown';
import {CalculationType} from "../../../src/components/Calculation";
import moment from 'moment';

describe(`CalculationBreakdown.vue Tests`, () => {
    it(`shows the payment breakdowns, if it has them`, () => {
        const expectedPaymentName = 'Test Payment';
        const otherExpectedPaymentName = 'Other Test Payment';
        const value = 800;
        const breakdown = {
            name: 'Test Breakdown',
            payments: [
                {
                    name: expectedPaymentName,
                    value
                },
                {
                    name: otherExpectedPaymentName,
                    value
                }
            ],
            endMoment: moment(),
            amortization: [],
            balance: 100
        };

        const wrapper = shallowMount(CalculationBreakdown, {
            propsData: {
                breakdown
            }
        });

        const paymentNames = wrapper.findAll('ol small').wrappers;
        const paymentValues = wrapper.findAll('ol span').wrappers;

        expect(paymentNames[0].text()).toBe(expectedPaymentName);
        expect(paymentValues[0].text()).toContain(value);
        expect(paymentNames[1].text()).toBe(otherExpectedPaymentName);
        expect(paymentValues[1].text()).toContain(value);
    });

    it(`shows loan specific information, when relevant`, () => {
        const breakdown = {
            name: 'Test Breakdown',
            type: CalculationType.LOAN,
            amortization: [],
            balance: 100
        };

        const wrapper = shallowMount(CalculationBreakdown, {
            propsData: {
                breakdown
            }
        });

        expect(wrapper.html().toLowerCase()).toContain('principal');
        expect(wrapper.html().toLowerCase()).toContain('interest');
        expect(wrapper.html().toLowerCase()).toContain('total');
    });

    it(`shows savings specific information, when relevant`, () => {
        const breakdown = {
            name: 'Test Breakdown',
            type: CalculationType.SAVINGS,
            amortization: [],
            balance: 100
        };

        const wrapper = shallowMount(CalculationBreakdown, {
            propsData: {
                breakdown
            }
        });

        expect(wrapper.html().toLowerCase()).toContain('contribution');
        expect(wrapper.html().toLowerCase()).toContain('balance');
        expect(wrapper.html().toLowerCase()).toContain('saved');
    });

    it('label of time should be years if 12 months', () => {
        const breakdown = {
            endMoment: moment().add(13, 'months')
        };

        const wrapper = shallowMount(CalculationBreakdown, {
            propsData: {
                breakdown
            }
        });

        expect(wrapper.vm.labelOfTime.toLowerCase()).toBe('years');
    });

    it('label of time should be months if 11 months', () => {
        const breakdown = {
            endMoment: moment().add(12, 'months')
        };

        const wrapper = shallowMount(CalculationBreakdown, {
            propsData: {
                breakdown
            }
        });

        expect(wrapper.vm.labelOfTime.toLowerCase()).toBe('months');
    })
});

import Vue from 'vue';
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faArrowLeft, faArrowRight, faPlus, faTimes} from "@fortawesome/free-solid-svg-icons";
import {shallowMount} from "@vue/test-utils";
import Calculation, {CalculationType, CalculationSubType} from '../../../src/components/Calculation';
import {CalculationModel} from "../../../src/components/Calculator";

library.add(faPlus, faArrowLeft, faArrowRight, faTimes);
Vue.component('font-awesome-icon', FontAwesomeIcon);

const showSavingsCalculation = (wrapper) => {
    const savingsBtn = wrapper.find('.savings-btn');
    savingsBtn.trigger('click');
};

const showLoanCalculation = (wrapper) => {
    const loanBtn = wrapper.find('.loan-btn');
    loanBtn.trigger('click');
};

describe(`Calculation.vue Tests`, () => {
    // Inspect the component instance on mount
    it('defaults to loan', () => {
        const wrapper = shallowMount(Calculation, {
            propsData: {
                index: 0,
                lastIndex: 0,
                calculation: new CalculationModel(),
                minValue: 10,
                maxValue: 100,
            }
        });
        const button = wrapper.find('.active-btn');
        expect(button.text()).toContain('Loan');
    });

    it('shows the savings calculation, when savings tab is clicked', () => {
        const wrapper = shallowMount(Calculation, {
            propsData: {
                index: 0,
                lastIndex: 0,
                calculation: new CalculationModel(),
                minValue: 10,
                maxValue: 100,
            }
        });
        showSavingsCalculation(wrapper);
        const button = wrapper.find('.active-btn');
        expect(button.text()).toContain('Savings');
    });

    it('#6 Loan subtype retains checked status when moved from left to right.', () => {
        const wrapper = shallowMount(Calculation, {
            propsData: {
                index: 1,
                lastIndex: 1,
                calculation: new CalculationModel(),
                minValue: 10,
                maxValue: 100,
            }
        });
        const subType = new CalculationSubType();
        const autoSelection = wrapper.find(`input[value="${subType.LOAN_AUTO}"]`);
        // click Auto Loan input
        autoSelection.trigger('click');
        // Change index to 0 by moving it. This is where the issue existed.
        // It would lose it's checked status because of the `name` attribute.
        const moveLeftBtn = wrapper.find('nav button.move-left');
        moveLeftBtn.trigger('click');
        // Assert that the auto loan value is checked.
        expect(autoSelection.element.checked).toBeTruthy()
    })

    describe('@events', () => {
        let wrapper;
        let index;

        beforeEach(() => {
            index = 1;
            wrapper = shallowMount(Calculation, {
                propsData: {
                    index,
                    lastIndex: 3,
                    calculation: new CalculationModel(),
                    minValue: 10,
                    maxValue: 100,
                }
            });
        });

        afterEach(() => {
            index = undefined;
            wrapper = undefined;
        });

        it('@interest-rate-change - should emit an "interest-rate-change" event when type is changed to savings', () => {
            showSavingsCalculation(wrapper);
            expect(wrapper.emitted()['interest-rate-change'][0]).toEqual([null, index, true]);
        });
        it('@interest-rate-change - should not emit an "interest-rate-change" event when type is changed to loan', () => {
            showSavingsCalculation(wrapper);
            showLoanCalculation(wrapper);
            // When savings was clicked
            expect(wrapper.emittedByOrder()[0].name).toBe('interest-rate-change');
            expect(wrapper.emittedByOrder()[1].name).toBe('sub-type-change');
            expect(wrapper.emittedByOrder()[2].name).toBe('type-change');
            // When loan was clicked
            expect(wrapper.emittedByOrder()[3].name).toBe('sub-type-change');
            expect(wrapper.emittedByOrder()[4].name).toBe('type-change');
        });
        it('@type-change - should emit an "type-change" event when type is changed', () => {
            showSavingsCalculation(wrapper);
            expect(wrapper.emitted()['type-change'][0]).toEqual([CalculationType.SAVINGS, index]);
        });
        it('@change-position - should emit an "position-change" event when left arrow is clicked', () => {
            const moveLeftBtn = wrapper.find('nav button.move-left');
            moveLeftBtn.trigger('click');
            expect(wrapper.emitted()['change-position'][0]).toEqual([index, 0]);
        });

        it('@change-position - should emit an "position-change" event when right arrow is clicked', () => {
            const moveRightBtn = wrapper.find('nav button.move-right');
            moveRightBtn.trigger('click');
            expect(wrapper.emitted()['change-position'][0]).toEqual([index, 2]);
        });

        it('@remove-calculation - should emit a "remove-calculation" event when close button is clicked', () => {
            const moveRightBtn = wrapper.find('button.remove-calculation');
            moveRightBtn.trigger('click');
            expect(wrapper.emitted()['remove-calculation']).toBeDefined();
        });

        it('@keypress - should not allow bad input, like letters', () => {
            const loanBalance = wrapper.find('.loan-balance');
            loanBalance.trigger('keypress', {
                key: 'a'
            });
            expect(wrapper.emitted()['balance-change']).toBeUndefined();
        });

        it('@balance-change - should emit "balance-change" when loan balance is changed', () => {
            const value = 1;
            const loanBalance = wrapper.find('.loan-balance');
            loanBalance.setValue(value);
            loanBalance.trigger('keypress');
            expect(wrapper.emitted()['balance-change'][0]).toEqual([value.toString(), CalculationType.LOAN, index]);
        });

        it('@balance-change - should emit "balance-change" when savings balance is changed', () => {
            const value = 1;
            showSavingsCalculation(wrapper);
            const loanBalance = wrapper.find('.savings-balance');
            loanBalance.setValue(value);
            loanBalance.trigger('keypress');
            expect(wrapper.emitted()['balance-change'][0]).toEqual([value.toString(), CalculationType.SAVINGS, index]);
        });

        it('@interest-rate-change - should emit "interest-rate-change" when loan interest rate is changed', () => {
            const value = 1;
            const interestRate = wrapper.find('.interest-rate');
            interestRate.setValue(value);
            interestRate.trigger('keypress');
            expect(wrapper.emitted()['interest-rate-change'][0]).toEqual([value.toString(), index]);
        });

        it('@payment-change - should emit "payment-change" when loan balance is changed', () => {
            const value = 1;
            const interestRate = wrapper.find('.loan-payment');
            interestRate.setValue(value);
            interestRate.trigger('keypress');
            expect(wrapper.emitted()['payment-change'][0]).toEqual([value.toString(), CalculationType.LOAN, index]);
        });

        it('@payment-change - should emit "payment-change" when savings payment is changed', () => {
            const value = 1;
            showSavingsCalculation(wrapper);
            const loanBalance = wrapper.find('.savings-payment');
            loanBalance.setValue(value);
            loanBalance.trigger('keypress');
            expect(wrapper.emitted()['payment-change'][0]).toEqual([value.toString(), CalculationType.SAVINGS, index]);
        });

        it('@goal-change - should emit "goal-change" when savings goal is changed', () => {
            const value = 1;
            showSavingsCalculation(wrapper);
            const loanBalance = wrapper.find('.savings-goal');
            loanBalance.setValue(value);
            loanBalance.trigger('keypress');
            expect(wrapper.emitted()['goal-change'][0]).toEqual([value.toString(), CalculationType.SAVINGS, index]);
        });
    })
});
